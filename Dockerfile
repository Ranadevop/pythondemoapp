FROM python:3.9-alpine
WORKDIR .
COPY /test.py .

CMD python test.py
